
import base64
import os
from datetime import date
from datetime import datetime
from datetime import *
import datetime
from odoo.tools.float_utils import float_round
from dateutil.relativedelta import relativedelta

from io import BytesIO
import xlsxwriter
from PIL import Image as Image
from odoo import fields, models, api, _
from odoo.exceptions import ValidationError
from xlsxwriter.utility import xl_rowcol_to_cell

    
#CODIGO AGREGADO POR ARIEL CERRRATO CODIGO BUENO.

class payroll_report_excel(models.TransientModel):
    _name = 'payroll.report.excel'

    name = fields.Char('File Name', size=256, readonly=True)
    file_download = fields.Binary('Download payroll', readonly=True)

class Rule(models.Model):
    _inherit = 'hr.salary.rule'

    add_rule_ids = fields.Many2many('hr.salary.rule',
                                    relation='add_rule_tbl',
                                    column1='r_id',
                                    column2='r1_id',
                                    string='Add Rules')
    sub_rule_ids = fields.Many2many('hr.salary.rule',
                                    relation='sub_rule_tbl',
                                    column1='r_id',
                                    column2='r1_id',
                                    string='Sub Rules')

class hr_payslip(models.Model):
    _inherit = 'hr.payslip'

    @api.one
    def get_amount_from_rule_code(self, rule_code):
        line = self.env['hr.payslip.line'].search([('slip_id', '=', self.id), ('code', '=', rule_code)])
        if line:
            return round(line.total, 2)
        else:
            return 0.0

    @api.one
    def update_sheet(self):
        for slip_line in self.env['hr.payslip.line'].search([('slip_id', '=', self.id)]):
            final_total = 0
            if slip_line.salary_rule_id.add_rule_ids or slip_line.salary_rule_id.sub_rule_ids:
                for add_line in slip_line.salary_rule_id.add_rule_ids:
                    line = self.env['hr.payslip.line'].search([('slip_id', '=', self.id),
                                 ('salary_rule_id', '=', add_line.id)])
                    if line:
                        final_total += line.rate * line.amount * line.quantity / 100
                for sub_line in line.salary_rule_id.sub_rule_ids:
                    line = self.search([('slip_id', '=', self.id),
                                 ('salary_rule_id', '=', sub_line.id)])
                    if line:
                        final_total -= line.rate * line.amount * line.quantity / 100
                slip_line.amount = final_total

   
    @api.one
    def compute_sheet(self):
        if not self.line_ids:
            super(hr_payslip, self).compute_sheet()
        self.update_sheet()
        return True

class PayslipBatches(models.Model):
    _inherit = 'hr.payslip.run'

    #name = fields.Char('File Name', size=256, readonly=True)
    file_data = fields.Binary('File')


    @api.multi
    def get_all_columns(self):
        result = {}
        all_col_list_seq = []
        if self.slip_ids:
            for line in self.env['hr.payslip.line'].search([('slip_id', 'in', self.slip_ids.ids)], order="sequence"):
                if line.code not in all_col_list_seq:
                    all_col_list_seq.append(line.code)
                if line.code not in result.keys():
                    result[line.code] = line.name
        return [result, all_col_list_seq]

    #Suma de las horas extras que esten validadas y que tenga como fechas la de inicio y al del fin
    @api.multi
    def duracion_fechas(self):
        total = []
        to = 111.0
        record_ids = self.env['hr.payslip.line'].search([('slip_id', 'in', self.slip_ids.ids)]) 
        for record in record_ids:
            if record.code == 'NET':
                record.write({
                    'amount': '222'
                })
        return record_ids

        #for netu in vaca_validacion:
        #    if netu.code == 'NET':
        #        result = super(PayslipBatches, self).write({'amount': to})
        
   #ULTIMOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
    @api.multi
    def get_nomi_data(self):
        file_name = _('payroll report.xlsx')
        fp = BytesIO()

        workbook = xlsxwriter.Workbook(fp)
        heading_format = workbook.add_format({'align': 'center',
                                              'valign': 'vcenter',
                                              'bold': True, 
                                              'size': 12,
                                              'font_color': 'white',
                                              'bg_color' : 'blue'
                                              })
        cell_text_format_n = workbook.add_format({'align': 'left',
                                                  'bold': True, 'size': 13,
                                                  })
        cell_text_format = workbook.add_format({'align': 'center',
                                                'bold': True, 'size': 9,
                                                })

        cell_text_format.set_border()
        cell_text_format_new = workbook.add_format({'align': 'center',
                                                    'size': 9,
                                                    })
        cell_text_format_new.set_border()
        cell_number_format = workbook.add_format({'align': 'center',
                                                  'bold': False, 'size': 9,
                                                  'num_format': 'L         #,##0.00'})
        cell_number_format.set_border()
        worksheet = workbook.add_worksheet('payroll report.xlsx')
        normal_num_bold = workbook.add_format({'bold': True, 'num_format': '#,###0.00', 'size': 9, })
        normal_num_bold.set_border()
        worksheet.set_column('A:A', 20)
        worksheet.set_column('B:B', 20)
        worksheet.set_column('C:C', 20)
        worksheet.set_column('D:D', 20)
        worksheet.set_column('E:E', 20)
        worksheet.set_column('F:F', 20)
        worksheet.set_column('G:G', 20)
        worksheet.set_column('H:H', 20)
        worksheet.set_column('I:I', 20)
        worksheet.set_column('J:J', 20)
        worksheet.set_column('K:K', 20)
        worksheet.set_column('L:L', 20)
        worksheet.set_column('M:M', 20)
        worksheet.set_column('N:N', 20)

        #date_2 = datetime.strftime(self.date_end, '%Y-%m-%d %H:%M:%S')
        #date_1= datetime.strftime(self.from_date, '%Y-%m-%d %H:%M:%S')
        #payroll_month = self.from_date.strftime("%B")

        #worksheet.merge_range('A1:F2', 'Payroll For %s %s' % (payroll_month, self.from_date.year), heading_format)
        #INSERTAR IMAGEN DEL LOGO EN EL DOCUMENTO DE EXCEL, AMTES DE REALIZAR TIENE QUE ESTAR EL LOGO
        logo = self.env.user.company_id.logo
        buf_image= BytesIO(base64.b64decode(logo))
        x_scale = 0.43
        y_scale = 0.15
        worksheet.insert_image('A1', "any_name.png", {'image_data': buf_image, 'y_scale': y_scale, 'x_scale': x_scale, 'object_position':4})

        row = 2
        column = 0
        
        ini = str(self.date_start)
        fini = str(self.date_end)
        nombre_empre = str(self.env.user.company_id.name)
        #worksheet.merge_range('B5:D5', '%s' % (self.env.user.company_id.name), cell_text_format_n)    
        worksheet.write('E1',  'Empresa',  cell_text_format_n)
        worksheet.write('F1',  nombre_empre)
        row += 1
        worksheet.write('E2', 'Fecha Inicial',  cell_text_format_n)
        worksheet.write('F2', ini)
        row += 1
        worksheet.write('E3', 'Fecha Final', cell_text_format_n)
        worksheet.write('F3', fini)
        row += 2
        res=self.get_all_columns()
        all_col_nombre = res[0]
        all_col_codigo = res[1]

        row = 6

        worksheet.write(row, 0, 'REFERENCIA', heading_format)
        worksheet.write(row, 1, 'CODIGO', heading_format)
        worksheet.write(row, 2, 'NOMBRE COMPLETO', heading_format)
        worksheet.write(row, 3, 'CARGO QUE DESEMPEÑA', heading_format)
        worksheet.write(row, 4, 'DEPARTAMENTO', heading_format)
        #worksheet.write(row, 5, 'CORREO', heading_format)
        #worksheet.write(row, 6, 'CUENTA BANCARIA', heading_format)
        worksheet.write(row, 5, 'FECHA INGRESO', heading_format)
        worksheet.write(row, 6, 'DIAS LABORADOS', heading_format)
        worksheet.write(row, 7, 'HORAS EXTRAS', heading_format)
        worksheet.write(row, 8,'SUEDO POR HORA', heading_format)
        worksheet.write(row, 9,'TOTAL HORAS', heading_format)
        worksheet.write(row, 10,'DIAS NO TRABAJADOS', heading_format)
        worksheet.write(row, 11,'SUELDO QUINCENAL', heading_format)
        
        #worksheet.write(row, 19,'ISR', heading_format)
        #worksheet.write(row, 28,'ISR', heading_format)

        row_set = row
        column = 12
        #Nombre de las reglas salariales como titulo
        for vals in all_col_codigo:
            worksheet.write(row, column, all_col_nombre[vals], heading_format)
            column += 1

        row = 7
        for slip in self.slip_ids:
            worksheet.write(row, 0, str(slip.number), cell_text_format)
            worksheet.write(row, 1, str(slip.employee_id.id), cell_text_format)
            worksheet.write(row, 2, str(slip.employee_id.name), cell_text_format)
            cargo = slip.employee_id.job_id.name or None
            worksheet.write(row, 3, str(cargo), cell_text_format)
            dept_nm = slip.employee_id.department_id and slip.employee_id.department_id.name or None
            job_nm = slip.employee_id.work_email or None
            worksheet.write(row, 4, dept_nm, cell_text_format)
            #worksheet.write(row, 5, str(job_nm), cell_text_format)
            #worksheet.write(row, 6, str(slip.employee_id.bank_account_id.acc_number), cell_text_format)
        
            #total_work_days = 0
            #for workline in slip.worked_days_line_ids:
            #    if workline.code.upper() != 'UNPAID':
            #        total_work_days += workline.number_of_days
           
            

            #Suma del total de dias * las horas de extras aprobadas
            total_horas = 0.0
            total_horas_arre = {}
            #VARIABLES DE VALIDACION DEL TOTAL DE HORAS TRABAJADAS 
            total_horas_asis = 0.0
            total_horas_asisten = {}
            #total deducciones
            total_deducciones = {}
            nombre_deducciones = {}
            #total INGRESOS
            total_ingresos = {}
            nombre_ingresos = {}
            #Agarra si el empleado es permanente o por hora
            tipo_emple = {}
            hora_4 = 4.0
            hora_5 = 5.0
            hora_6 = 6.0
            hora_8 = 8.0

            hasta = self.date_end
            desde = self.date_start
            # Calculamos la diferencia de los días
            dias_totales = (hasta - desde).days
            total_d = int(dias_totales) + 1 
            
            #CALCULO DE VACACIONES PAGADAS E INPAGADAS
            pagadas = 0.0
            inpagadas = 0.0
            total_dias_trabajados = {}
            total_dias_no_trabajo = {}
            lista = []
            tr = True
            vaca_validacion = self.env['hr.leave'].search([('employee_id.id', '=', slip.employee_id.id),('state', '=', 'validate')]) 
            for natu in vaca_validacion:
                for days in range(dias_totales + 1): 
                    fecha = desde + relativedelta(days=days)
                    if natu['request_date_from'] == fecha:
                        if natu.holiday_status_id.unpaid == tr: 
                           inpagadas += natu['number_of_days']
                        else:
                           pagadas += natu['number_of_days']
                total_dias_trabajados[slip.employee_id.id] = pagadas
                total_dias_no_trabajo[slip.employee_id.id] = inpagadas
            
            #TOTAL HORAS
            contrato_validacion = self.env['hr.contract'].search([('employee_id', '=', slip.employee_id.id), ('emple_perma', '=', tr), ('state', '=', 'open')])
            paga = total_dias_trabajados.get(slip.employee_id.id) or 0.0
            inpa = total_dias_no_trabajo.get(slip.employee_id.id) or 0.0
            #DIAS NO PAGADOS
            worksheet.write(row, 10, str(inpa), cell_text_format)
            dias_comple = (total_d - inpa)
            worksheet.write(row, 6, str(dias_comple), cell_number_format)
            
            valor_final  = 0.0               
            contrato_hora = self.env['hr.contract'].search([('employee_id.id', '=', slip.employee_id.id), ('state', '=', 'open')])
            asistencia = self.env['hr.attendance'].search([('employee_id.id', '=', slip.employee_id.id)])
            saba = ''
            sap = 'SÁBADO'
            
            #FECHA INGRESO
            fecha_inn = contrato_hora.fecha_ingreso
            worksheet.write(row, 5, str(fecha_inn), cell_text_format)
            
            for datum in asistencia:
                mos = datum['check_in']
                hora_entrada = datetime.date(mos.year, mos.month, mos.day)
                saba = hora_entrada.strftime('%A').upper()
                for contra in contrato_hora:
                    tipo_emple[slip.employee_id.id] = contra['emple_perma']
                    if contra['hora_contractual'] == '4': 
                        for days in range(dias_totales + 1): 
                            fecha = desde + relativedelta(days=days)
                            if hora_entrada == fecha:
                                if datum['worked_hours'] >= 4:
                                        total_horas_asis += hora_4  
                                else:
                                    total_horas_asis += datum['worked_hours']
                        total = float(paga) * float(hora_4)
                        total1 = float(inpa) * float(hora_4)
                        valor_final = float(total_horas_asis) + float(total) - float(total1)
                    if contra['hora_contractual'] == '6': 
                        for days in range(dias_totales + 1): 
                            fecha = desde + relativedelta(days=days)
                            if hora_entrada == fecha:
                                if saba == sap: 
                                    if datum['worked_hours'] >= 4:
                                            total_horas_asis += hora_4  
                                    else:
                                        total_horas_asis += datum['worked_hours']
                                else: 
                                    if datum['worked_hours'] >= 5:
                                            total_horas_asis += hora_5  
                                    else:
                                        total_horas_asis += datum['worked_hours']
                        total = float(paga) * float(hora_5)
                        total1 = float(inpa) * float(hora_5)
                        valor_final = float(total_horas_asis) + float(total) - float(total1)
                    if contra['hora_contractual'] == '6': 
                        for days in range(dias_totales + 1): 
                            fecha = desde + relativedelta(days=days)
                            if hora_entrada == fecha:
                                if saba == sap: 
                                    if datum['worked_hours'] >= 4:
                                            total_horas_asis += hora_4  
                                    else:
                                        total_horas_asis += datum['worked_hours']
                                else: 
                                    if datum['worked_hours'] >= 6:
                                            total_horas_asis += hora_6  
                                    else:
                                        total_horas_asis += datum['worked_hours']
                        total = float(paga) * float(hora_6)
                        total1 = float(inpa) * float(hora_6)
                        valor_final = float(total_horas_asis) + float(total) - float(total1)
                    if contra['hora_contractual'] == '8': 
                        for days in range(dias_totales + 1): 
                            fecha = desde + relativedelta(days=days)
                            if hora_entrada == fecha:
                                if saba == sap: 
                                    if datum['worked_hours'] >= 4:
                                            total_horas_asis += hora_4  
                                    else:
                                        total_horas_asis += datum['worked_hours']
                                else: 
                                    if datum['worked_hours'] >= 8:
                                            total_horas_asis += hora_8  
                                    else:
                                        total_horas_asis += datum['worked_hours']
                        total = float(paga) * float(hora_8)
                        total1 = float(inpa) * float(hora_8)
                        valor_final = float(total_horas_asis) + float(total) - float(total1)
                    if contra['hora_contractual'] == 'Permanente': 
                        for days in range(dias_totales + 1): 
                            fecha = desde + relativedelta(days=days)
                            if hora_entrada == fecha:
                                if saba == sap: 
                                    if datum['worked_hours'] >= 4:
                                            total_horas_asis += hora_4  
                                    else:
                                        total_horas_asis += datum['worked_hours']
                                else: 
                                    if datum['worked_hours'] >= 8:
                                            total_horas_asis += hora_8  
                                    else:
                                        total_horas_asis += datum['worked_hours']
                        total = float(paga) * float(hora_8)
                        total1 = float(inpa) * float(hora_8)
                        valor_final = float(total_horas_asis) + float(total) - float(total1)
                total_horas_asisten[slip.employee_id.id] = valor_final
                
            tot_sueld = 0.0
            ace = 'aprobado'
            #Variables hora normal
            ace2 = True
            tinormal = 'horanormal'
            hora_normal = self.env['test_model_precio'].search([('horas_activo', '=', ace2),('tipo_hora', '=', tinormal)], limit=1)
            #Variable hora vacaciones
            tivacaciones = 'vacaciones'
            hora_vacacio = self.env['test_model_precio'].search([('horas_activo', '=', ace2),('tipo_hora', '=', tivacaciones)], limit=1)
            stage_asisten = self.env['test_model_name'].search([('employee_id', '=', slip.employee_id.id),('fase_horas', '=', ace)]) 
            vacio = 0.0
            for workline in slip.worked_days_line_ids:       
                if stage_asisten:
                    for datum in stage_asisten:
                        for days in range(dias_totales + 1): 
                            fecha = desde + relativedelta(days=days)
                            if datum['fecha'] == fecha: 
                               total_horas += datum['hora_extra']
                        worksheet.write(row, 7, str(total_horas), cell_number_format)
                        total_ex_tr = float(total_horas) + float(valor_final)
                        worksheet.write(row, 9, total_ex_tr, cell_text_format)
                            #Calculo del total de horas * precio de hora
                        if datum['horas_vaca'] == True:
                            tocon = float(total_horas) * float(hora_vacacio['hora_lps'])
                            worksheet.write(row, 8, hora_normal['hora_lps'], cell_number_format)
                            pal = float_round(tocon, precision_digits=2)                      
                            total_horas_arre[slip.employee_id.id] = pal
                        else:
                            tocon = float(total_horas) * float(hora_normal['hora_lps'])
                            worksheet.write(row, 8, hora_normal['hora_lps'], cell_number_format)                      
                            pal = float_round(tocon, precision_digits=2)
                            total_horas_arre[slip.employee_id.id] = pal   
                else:
                    worksheet.write(row, 7, str(total_horas), cell_number_format)
                    total_ex_tr = float(total_horas) + float(valor_final)
                    worksheet.write(row, 9, str(total_ex_tr), cell_text_format)
                    worksheet.write(row, 8, hora_normal['hora_lps'], cell_number_format)
                    #worksheet.write(row, 10, str(vacio), cell_number_format)
            if contrato_validacion:
               total_hor = total_horas_asisten.get(slip.employee_id.id)
               total_extra = total_horas_arre.get(slip.employee_id.id)
               tot_sueld = contrato_validacion.wage
               sueldo_neto = (contrato_validacion.wage/2)
               t1 = float(sueldo_neto) + float(total_extra or 0.0)
               worksheet.write(row, 11, t1, cell_number_format) 
            else:
               total_hor = total_horas_asisten.get(slip.employee_id.id)
               total_extra = total_horas_arre.get(slip.employee_id.id)
               tot_sueld = contrato_validacion.wage
               sueldo_neto = float(total_hor) * float(hora_normal['hora_lps'])
               t1 = float(sueldo_neto) + float(total_extra or 0.0)
               worksheet.write(row, 11, t1, cell_number_format) 
            code_col = 12

           
            
            
            #Ingresos
            tot_ingr = 0.0
            total_ingre = 0.0
            to_ingre1 = 0.0
            to_ingre2 = 0.0
            to_ingre3 = 0.0
            to_ingre4 = 0.0
            to_ingre5 = 0.0
            ingre_emple = self.env['test_model_ingresos'].search([('tipo_ingre_id.category_id.code', '=', 'INGRE'), ('employee_id', '=', slip.employee_id.id)])
            if ingre_emple:     
                for petu in ingre_emple:
                    for days in range(dias_totales + 1): 
                        fecha = desde + relativedelta(days=days)
                        if petu.fecha_precio == fecha:
                            total_ingre += petu['monto_lps']
                            if petu.tipo_ingre_id.code == 'COMI':
                                to_ingre1 += petu['monto_lps']
                            if petu.tipo_ingre_id.code == 'OTR_INGRE':
                                to_ingre2 += petu['monto_lps']   
                            if petu.tipo_ingre_id.code == 'BNTRA':
                                to_ingre3 += petu['monto_lps']
                            if petu.tipo_ingre_id.code == 'BONIFI':
                                to_ingre4 += petu['monto_lps']   
                            if petu.tipo_ingre_id.code == 'VACA':
                                to_ingre5 += petu['monto_lps']
                    worksheet.write(row, 12, to_ingre1, cell_number_format)
                    worksheet.write(row, 13, to_ingre2, cell_number_format)
                    worksheet.write(row, 14, to_ingre3, cell_number_format)
                    worksheet.write(row, 15, to_ingre4, cell_number_format)
                    worksheet.write(row, 16, to_ingre5, cell_number_format)
                    if contrato_validacion:
                        sueldo = (contrato_validacion.wage/2)
                        hora = total_horas_arre.get(slip.employee_id.id)
                        if hora:
                            tot_ingr = float(sueldo) + float(total_ingre) + float(hora)
                            va_in = float_round(tot_ingr, precision_digits=2)
                            worksheet.write(row, 17, va_in, cell_number_format) 
                            pal = float_round(total_ingre, precision_digits=2)
                            total_ingresos[slip.employee_id.id] = pal
                        else:
                            tot_ingr = float(sueldo) + float(total_ingre)
                            va_in = float_round(tot_ingr, precision_digits=2)
                            worksheet.write(row, 17, va_in, cell_number_format) 
                            pal = float_round(total_ingre, precision_digits=2)
                            total_ingresos[slip.employee_id.id] = pal
                    else:               
                        hora = total_horas_arre.get(slip.employee_id.id)
                        if hora:
                            tot_ingr = float(sueldo_neto) + float(total_ingre) + float(hora) 
                            va_in = float_round(tot_ingr, precision_digits=2)
                            worksheet.write(row, 17, va_in, cell_number_format) 
                            pal = float_round(total_ingre, precision_digits=2)
                            total_ingresos[slip.employee_id.id] = pal
                        else:
                            tot_ingr = float(sueldo_neto) + float(total_ingre) 
                            va_in = float_round(tot_ingr, precision_digits=2)
                            worksheet.write(row, 17, va_in, cell_number_format) 
                            pal = float_round(total_ingre, precision_digits=2)
                            total_ingresos[slip.employee_id.id] = pal
            else:
                worksheet.write(row, 12, to_ingre1, cell_number_format)
                worksheet.write(row, 13, to_ingre2, cell_number_format)
                worksheet.write(row, 14, to_ingre3, cell_number_format)
                worksheet.write(row, 15, to_ingre4, cell_number_format)
                worksheet.write(row, 16, to_ingre5, cell_number_format)
                hora = total_horas_arre.get(slip.employee_id.id)    
                if hora:
                    tot_ingr = float(sueldo_neto) + float(total_ingre) + float(hora)
                    va_in = float_round(tot_ingr, precision_digits=2)
                    worksheet.write(row, 17, va_in, cell_number_format) 
                    total_ingresos[slip.employee_id.id] = 0.0
                else:
                    tot_ingr = float(sueldo_neto) + float(total_ingre)
                    va_in = float_round(tot_ingr, precision_digits=2)
                    worksheet.write(row, 17, va_in, cell_number_format) 
                    total_ingresos[slip.employee_id.id] = 0.0
            
            #CALCULO DEL RAP
            to_rap = float_round((tot_sueld - 9792.74), precision_digits=2)
            to_raa =  float_round((to_rap * 0.015), precision_digits=2)
            total_rap = float_round((to_raa/2), precision_digits=2)  
            
            #CALCULO DEL IHSS 376.33 
            to_ihss = 376.33

            #CALCULO DEL ISR     
            tosumaa = to_raa + to_ihss

            
            
            #CALCULO DEL ISR     
            deduccion = 0.0
            total_isr = 0.0
            nsu = (contrato_validacion.wage * 12)- 40000
            gravable = (nsu - tosumaa) 
            if gravable > 0.01 and gravable <= 165482.06: 
               total_isr = 0.0 
            else: 
                    if gravable>165482.06 and gravable <=252330.80: 
                       deduccion = (gravable -165482.06)*0.15 
                    else: 
                        if gravable > 252330.81 and gravable <= 586815.84: 
                           deduccion = (86848.74*0.15) + (gravable-252330.80)*0.20 
                        else: 
                            if gravable > 586815.84: 
                                deduccion = (86848.74*0.15) + (334485.03*0.20) + (gravable-586815.84) * 0.25 
                    total_isr = (deduccion/12)
            
            #Deducciones 
            total_dedu = 0.0
            nom_dedu = ''
            #Deducciones detalladas
            to_descu1 = (to_ihss/2)
            to_descu2 = 0.0
            to_descu3 = 0.0
            to_descu4 = 0.0
            to_descu5 = 0.0
            to_descu6 = 0.0
            to_descu7 = 0.0
            to_descu8 = 0.0
            pal_final = 0.0
            va_dedu = 0.0
            cod_regla = self.env['hr.payslip.line'].search([('slip_id', '=', slip.id)])
            dedu_emple = self.env['test_model_deducciones'].search([('tipo_dedu_id.category_id.code', '=', 'DED'), ('employee_id', '=', slip.employee_id.id)])
            if dedu_emple:    
                for natu in dedu_emple:
                    for days in range(dias_totales + 1): 
                        fecha = desde + relativedelta(days=days)
                        if natu['fecha_precio'] == fecha:
                            total_dedu += natu['monto_lps']
                            if natu.tipo_dedu_id.code == 'INCAPA':
                                to_descu2 += natu['monto_lps']   
                            if natu.tipo_dedu_id.code == 'CAFEYA':
                                to_descu3 += natu['monto_lps']
                            if natu.tipo_dedu_id.code == 'CAFEYECA':
                                to_descu4 += natu['monto_lps']   
                            if natu.tipo_dedu_id.code == 'CAM1':
                                to_descu5 += natu['monto_lps']
                            if natu.tipo_dedu_id.code == 'LITEL':
                                to_descu6 += natu['monto_lps']
                            if natu.tipo_dedu_id.code == 'EQUIMO':
                                to_descu7 += natu['monto_lps'] 
                            if natu.tipo_dedu_id.code == 'OTRA_DEDU':
                                to_descu8 += natu['monto_lps']
                    worksheet.write(row, 18, to_descu1, cell_number_format)  
                    worksheet.write(row, 19, to_descu2, cell_number_format)
                    worksheet.write(row, 20, to_descu3, cell_number_format)
                    worksheet.write(row, 21, to_descu4, cell_number_format)
                    worksheet.write(row, 22, to_descu5, cell_number_format)
                    worksheet.write(row, 23, to_descu6, cell_number_format)
                    worksheet.write(row, 24, to_descu7, cell_number_format)
                    worksheet.write(row, 25, to_descu8, cell_number_format)
                    pal = float_round(total_isr, precision_digits=2)
                    pal_final = float_round((pal/2), precision_digits=2) 
                    worksheet.write(row, 26, pal_final, cell_number_format)
                    
                    suma_de = float(total_dedu) + float(pal_final)
                    va_dedu = float_round(suma_de, precision_digits=2)
                    worksheet.write(row, 27, va_dedu, cell_number_format)
                    total_deducciones[slip.employee_id.id] = va_dedu                            
                    code_col = 28
            else:    
                worksheet.write(row, 18, to_descu1, cell_number_format)  
                worksheet.write(row, 19, to_descu2, cell_number_format)
                worksheet.write(row, 20, to_descu3, cell_number_format)
                worksheet.write(row, 21, to_descu4, cell_number_format)
                worksheet.write(row, 22, to_descu5, cell_number_format)
                worksheet.write(row, 23, to_descu6, cell_number_format)
                worksheet.write(row, 24, to_descu7, cell_number_format)
                worksheet.write(row, 25, to_descu8, cell_number_format)
                pal = float_round(total_isr, precision_digits=2)
                pal_final = float_round((pal/2), precision_digits=2) 
                worksheet.write(row, 26, pal_final, cell_number_format)
                 
                suma_de = float(total_dedu) + float(pal_final)
                va_dedu = float_round(suma_de, precision_digits=2)
                worksheet.write(row, 27, va_dedu, cell_number_format)
                total_deducciones[slip.employee_id.id] = va_dedu                            
                code_col = 28       
            #VALIDACION DE VACACIONES
                
            
            #Modificar esta informacion luego es sumar las horas extras al total NET
            #total = 0.0
            va = 0.0
            for code in all_col_codigo:
                per = slip.get_amount_from_rule_code(code)[0]
                amt = (per/2) 
                perma = False
                if  code == 'NET':
                                    #ENTRA SI EL EMPLEADO TIENE HORAS EXTRAS
                            if total_horas_arre.get(slip.employee_id.id) != None:
                                        #Si empleado no es permanente entra
                                    if  tipo_emple.get(slip.employee_id.id) == perma:
                                        hora = total_horas_arre.get(slip.employee_id.id) 
                                        total = float(sueldo_neto) + float(hora) 
                                              #ENTRA SI TIENE MONTO EN DEDUCCIONES
                                        if total_deducciones.get(slip.employee_id.id) != None:
                                            monto_deducci = total_deducciones.get(slip.employee_id.id)
                                             #ENTRA SI TIENE INGRESOS
                                            if total_ingresos.get(slip.employee_id.id) != None:
                                               monto_ingre = total_ingresos.get(slip.employee_id.id)
                                               to_to = float(total) + float(monto_ingre) - float(monto_deducci) 
                                               va = float_round(to_to, precision_digits=2)
                                               worksheet.write(row, code_col, va, cell_number_format)
                                               code_col += 1
                                               #ENTRA SI NO TIENE INGRESOS
                                            else:
                                               to_to = float(total) - float(monto_deducci) 
                                               va = float_round(to_to, precision_digits=2)
                                               worksheet.write(row, code_col, va, cell_number_format)
                                               code_col += 1
                                              #ENTRA SI NO TIENE DEDUCCIONES
                                        else:
                                            if total_ingresos.get(slip.employee_id.id) != None:
                                               monto_ingre = total_ingresos.get(slip.employee_id.id)
                                               to_to = float(total) + float(monto_ingre)
                                               va = float_round(to_to, precision_digits=2)
                                               worksheet.write(row, code_col, va, cell_number_format)
                                               code_col += 1
                                            else:
                                                va = float_round(total, precision_digits=2)
                                                worksheet.write(row, code_col, va, cell_number_format)
                                                code_col += 1
                                                #ENTRA SI EL EMPLEADO  ES PERMANENTE
                                    else:
                                        hora = total_horas_arre.get(slip.employee_id.id) 
                                        total = float(amt) + float(hora)
                                                #ENTRA SI TIENE DEDUCCIONES
                                        if total_deducciones.get(slip.employee_id.id) != None:
                                            monto_deducci = total_deducciones.get(slip.employee_id.id)
                                                #ENTRA SI TIENE INGRESOS
                                            if total_ingresos.get(slip.employee_id.id) != None:
                                              monto_ingre = total_ingresos.get(slip.employee_id.id)
                                              to_to = float(total) + float(monto_ingre) - float(monto_deducci) 
                                              va = float_round(to_to, precision_digits=2)
                                              worksheet.write(row, code_col, va, cell_number_format)
                                              code_col += 1
                                              #ENTRA SI NO TIENE INGRESOS
                                            else:
                                                to_to = float(total) - float(monto_deducci) 
                                                va = float_round(to_to, precision_digits=2)
                                                worksheet.write(row, code_col, va, cell_number_format)
                                                code_col += 1
                                        else:
                                                #ENTRA SI TIENE INGRESOS
                                            if total_ingresos.get(slip.employee_id.id) != None:
                                               monto_ingre = total_ingresos.get(slip.employee_id.id)
                                               to_to = float(total) + float(monto_ingre)
                                               va = float_round(to_to, precision_digits=2)
                                               worksheet.write(row, code_col, va, cell_number_format)
                                               code_col += 1
                                               #ENTRA SI NO TIENE INGRESOS
                                            else:
                                               va = float_round(total, precision_digits=2)
                                               worksheet.write(row, code_col, va, cell_number_format)
                                               code_col += 1
                                #ENTRA SI EL EMPLEADO NO TIENE HORAS EXTRAS
                            else:
                                if tipo_emple.get(slip.employee_id.id) == perma:
                                    total = sueldo_neto
                                       #ENTRA SI TIENE DEDUCCIONES
                                    if total_deducciones.get(slip.employee_id.id) != None:
                                        monto_deducci = total_deducciones.get(slip.employee_id.id)
                                        #ENTRA SI TIENE INGRESOS
                                        if total_ingresos.get(slip.employee_id.id) != None:
                                           monto_ingre = total_ingresos.get(slip.employee_id.id)
                                           to_to = float(total) + float(monto_ingre) - float(monto_deducci)  
                                           va = float_round(to_to, precision_digits=2)
                                           worksheet.write(row, code_col, va, cell_number_format)
                                           code_col += 1
                                           #ENTRA SI NO TIENE INGRESOS
                                        else:
                                           monto_ingre = float(total) - float(monto_deducci)
                                           va = float_round(monto_ingre, precision_digits=2)
                                           worksheet.write(row, code_col, va, cell_number_format)
                                           code_col += 1
                                           #ENTRA SI NO TIENE DEDUCCIONES
                                    else:
                                         #ENTRA SI TIENE INGRESOS
                                        if total_ingresos.get(slip.employee_id.id) != None:
                                           monto_ingre = total_ingresos.get(slip.employee_id.id)
                                           to_to = float(total) + float(monto_ingre)
                                           va = float_round(to_to, precision_digits=2)
                                           worksheet.write(row, code_col, va, cell_number_format)
                                           code_col += 1
                                        else:
                                            va = float_round(total, precision_digits=2)
                                            worksheet.write(row, code_col, va, cell_number_format)
                                            code_col += 1
                                            #ENTRA SI EL EMPLEADO ES PERMANENTE     
                                else:
                                    if total_deducciones.get(slip.employee_id.id) != None:
                                        monto_deducci = total_deducciones.get(slip.employee_id.id)
                                          #ENTRA SI TIENE INGRESOS
                                        if total_ingresos.get(slip.employee_id.id) != None:
                                           monto_ingre = total_ingresos.get(slip.employee_id.id)
                                           to_to = float(amt) + float(monto_ingre) - float(monto_deducci)
                                           va = float_round(to_to, precision_digits=2)
                                           worksheet.write(row, code_col, va, cell_number_format)
                                           code_col += 1
                                        else:
                                            to_to = float(amt) - float(monto_deducci)
                                            va = float_round(to_to, precision_digits=2)
                                            worksheet.write(row, code_col, va, cell_number_format)
                                            code_col += 1
                                         #ENTRA SI NO TIENE DEDUCCIONES
                                    else:
                                        if total_ingresos.get(slip.employee_id.id) != None:
                                           monto_ingre = total_ingresos.get(slip.employee_id.id)
                                           to_to = float(amt) + float(monto_ingre)
                                           va = float_round(to_to, precision_digits=2)
                                           worksheet.write(row, code_col, va, cell_number_format)
                                           code_col += 1
                                        else:
                                            va = float_round(amt, precision_digits=2)
                                            worksheet.write(row, code_col, va, cell_number_format)
                                            code_col += 1  
                    #DEMAS REGLAS SALARIALES
                #else:           
                #    worksheet.write(row, code_col, str(amt), cell_text_format) 
                #    code_col += 1  
            
            row += 1

            #CAMBIO EN EL DETALLE DE NOMINA INDIVIDUAL
            dedu_det = total_deducciones.get(slip.employee_id.id)
            #ingre_det = total_ingresos.get(slip.employee_id.id)
            record_ids = self.env['hr.payslip.line'].search([('slip_id', 'in', self.slip_ids.ids),('employee_id.id', '=', slip.employee_id.id) ]) 
            for record in record_ids:
                #INGRESOS
                if record.code == 'COMI':
                    record.write({
                        'amount': to_ingre1
                    })
                if record.code == 'OTR_INGRE':
                    record.write({
                        'amount': to_ingre2
                    })
                if record.code == 'BNTRA':
                    record.write({
                        'amount': to_ingre3
                    })
                if record.code == 'BONIFI':
                    record.write({
                        'amount': to_ingre4
                    })
                if record.code == 'VACA':
                    record.write({
                        'amount': to_ingre5
                    })
                if record.code == 'TOTAL_INGRE':
                    record.write({
                        'amount': tot_ingr
                    })

                #DEDUCCIONES
                if record.code == 'IHSS':
                    record.write({
                        'amount': to_descu1
                    })
                if record.code == 'INCAPA':
                    record.write({
                        'amount': to_descu2
                    })
                if record.code == 'CAFEYA':
                    record.write({
                        'amount': to_descu3
                    })
                if record.code == 'CAFEYECA':
                    record.write({
                        'amount': to_descu4
                    })
                if record.code == 'CAM1':
                    record.write({
                        'amount': to_descu5
                    })
                if record.code == 'LITEL':
                    record.write({
                        'amount': to_descu6
                    })
                if record.code == 'EQUIMO':
                    record.write({
                        'amount': to_descu7
                    })
                if record.code == 'OTRA_DEDU':
                    record.write({
                        'amount': to_descu8
                    })
                if record.code == 'ISR':
                    record.write({
                        'amount': pal_final
                    })
                if record.code == 'TOT_DESC':
                    record.write({
                        'amount': va_dedu
                    })
                #SUELDO NETO
                if record.code == 'NET':
                    record.write({
                        'amount': va
                    })
                

        worksheet.write(row, 1, '', cell_number_format)
        worksheet.write(row, 2, '', cell_number_format)

        workbook.close()
        file_download = base64.b64encode(fp.getvalue())
        fp.close()
        self = self.with_context(default_name=file_name, default_file_download=file_download)

        return {
            'name': 'payroll report Download',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'payroll.report.excel',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': self._context,
        }

           